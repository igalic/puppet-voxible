# coding: utf-8
require 'puppet/face'

Puppet::Face.define(:mgmtgraph, '0.1.0') do
  license 'MIT'
  copyright 'Igor Galić', 2017
  author 'Igor Galić <igalic@brainsware.at>'
  summary 'Manage remote nodes via SSH by fetching facts, then locally compiling a catalog with them.'

  action :inventory do
    summary 'Fetch facts for given node and store under $vardir/yaml/<node>.yaml'

    when_invoked do |options|
      puts "ssh #{options} /opt/puppetlabs/bin/facter --yaml"
    end
  end

  action :compile do
    summary 'Compile JSON catalog for given node using facts under $vardir/yaml/<node>.yaml'

    when_invoked do |options|
      Puppet::Face[@name, @version].inventory
      Puppet::Face['catalog', '0.0'].find options
    end
  end

  action :"remote-apply" do
    summary 'Apply compiled catalog on remote node'

    when_invoked do |options|
      json_catalog = Puppet::Face[@name, @version].compile
      puts "#{json_catalog} | ssh #{options} puppet apply --catalog -"
    end
  end
end
