# Ansible-like Puppet mode

As a precursor to
implementing
[Ploy Puppet](https://gitlab.com/i.galic/ploy_puppet) [Felix Frank](http://ffrank.github.io/) [suggested](https://twitter.com/felis_rex/status/845060766172622849) to
write a Puppet Face for it. After putting a bit of thought into the matter, I
decided that this is actually a great idea!


After all, I've been using Puppet in a server-less mode for a over a year now,
but it always seemed cumbersome to have all modules and all _Data_ on the target
machine. My desire to move towards more immutable infrastructures would make
Configuration Management a one-shot task anyway — reducing it to a step in the
application's build-process.
